package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.data.DataSaveBase64Request;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public class DataSaveBase64Command extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-base64";

    @NotNull
    private final String DESC = "Save data to base64 file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SAVE BASE64 DATA]");
        @NotNull final DataSaveBase64Request request = new DataSaveBase64Request(getToken());
        getDomainEndpoint().saveDataBase64(request);
    }

}
