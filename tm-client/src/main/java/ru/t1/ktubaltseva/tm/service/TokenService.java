package ru.t1.ktubaltseva.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.service.ITokenService;

@Getter
@Setter
public class TokenService implements ITokenService {

    @Nullable
    private String token;

}
