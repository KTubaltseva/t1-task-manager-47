package ru.t1.ktubaltseva.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskBindToProjectRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private String projectId;

    public TaskBindToProjectRequest(@Nullable final String token) {
        super(token);
    }

    public TaskBindToProjectRequest(@Nullable final String token, @Nullable final String taskId, @Nullable final String projectId) {
        super(token);
        this.taskId = taskId;
        this.projectId = projectId;
    }

}
