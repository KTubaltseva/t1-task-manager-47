package ru.t1.ktubaltseva.tm.exception.auth;

public class LoginIncorrectException extends AbstractAuthException {

    public LoginIncorrectException() {
        super("Error! Login is incorrect...");
    }

}