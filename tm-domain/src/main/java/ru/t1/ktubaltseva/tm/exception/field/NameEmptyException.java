package ru.t1.ktubaltseva.tm.exception.field;

public class NameEmptyException extends AbstractFieldException {

    public NameEmptyException() {
        super("Error! Name is empty...");
    }

}