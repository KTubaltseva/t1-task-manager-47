package ru.t1.ktubaltseva.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.component.Bootstrap;

public class ServerApp {

    public static void main(@Nullable final String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}