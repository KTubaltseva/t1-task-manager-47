package ru.t1.ktubaltseva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.model.IUserRepository;
import ru.t1.ktubaltseva.tm.comparator.CreatedComparator;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Comparator;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        return "created";
    }

    @Override
    protected @NotNull Class<User> getClazz() {
        return User.class;
    }

    @Override
    public @NotNull User create(
            @NotNull final String login,
            @NotNull final String password
    ) throws Exception {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        return add(user);
    }

    @Override
    public @NotNull User create(
            @NotNull String login,
            @NotNull String password,
            @Nullable String email
    ) throws Exception {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setEmail(email);
        return add(user);
    }

    @Override
    public @NotNull User create(
            @NotNull String login,
            @NotNull String password,
            @NotNull Role role
    ) throws Exception {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(role);
        return add(user);
    }

    @Override
    public @Nullable User findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "FROM " + getClazz().getSimpleName() + " m " +
                "WHERE m.login = :login";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public @Nullable User findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "FROM " + getClazz().getSimpleName() + " m " +
                "WHERE m.email = :email";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExists(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExists(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
